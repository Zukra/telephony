<?php

$action = $_REQUEST["action"] ?: '';

if ($action == 'get_data') {

    $url = "http://192.168.107.220/api/api.php";
    $parameters = [
        'exten'          => $_REQUEST['exten'],
        'client'         => $_REQUEST['client'],
        'start_calldate' => date('Y-m-d\TH:i:s', strtotime($_REQUEST['dateFrom'])),
        'end_calldate'   => date('Y-m-d\TH:i:s', strtotime($_REQUEST['dateTo']))
    ];
    $parameters = array_diff($parameters, ['', '1970-01-01T00:00:00']);
    $params = http_build_query($parameters);

    $url .= '?' . $params;
    $url = urldecode($url);
    $result = json_decode(file_get_contents($url), true);

//    echo $url;

    ob_start();
    require __DIR__ . '/result.php';
    $content = ob_get_clean();
    echo $content;
}