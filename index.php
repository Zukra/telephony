<?php
header('Content-type: text/html; charset=utf-8');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Telephony</title>

    <link rel="stylesheet" href="src/css/bootstrap.css">
    <link rel="stylesheet" href="src/css/bootstrap-datetimepicker.min.css">

    <style>
        #app {
            font-family: 'Avenir', Helvetica, Arial, sans-serif;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            text-align: center;
            color: #2c3e50;
            margin-top: 60px;
        }

        h1, h2 {
            font-weight: normal;
        }

        td {
            text-align: left;
        }
    </style>
</head>
<body>
<div id="app" class="container">
    <h2 align="center" class="title_tab">Записи телефонных разговоров</h2>
    <form name='form' class="form-horizontal">

        <input type="hidden" name="action" value="get_data">
        <div class="row bord">
            <div class="col-md-6">

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-5">
                            <label for="exten" class="">Номер трубки
                                менеджера</label>
                        </div>
                        <div class="col-md-7">
                            <input type="text" name="exten" class="form-control"
                                   id="exten" value="">
                        </div>
                    </div>
                
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="client" class="">Номер входящий</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="client"
                                   id="client" value="">
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="form-group">
                <div class="col-md-12 text_left">
                    <div class='input-group date inline_b' id='dateFrom'
                         data-link-field="dateFrom">
                    <span><b>Период</b></span>
                    <label for="client" class="inline_b">с</label>
                        <label>
                            <input type='text' class="form-control dateFrom"
                                   value=""
                                   name="dateFrom"
                                   required/>
                        </label>
                        <span class="input-group-addon">
                                <span class="glyphicon glyphicon-remove"></span>
                            </span>
                        <span class="input-group-addon d_non">
                                <span class="glyphicon glyphicon-th"></span>
                            </span>
                        <input type="hidden" value=""/>
                    </div>

                    <div class='input-group date inline_b' id='dateTo'
                         data-link-field="dateTo">
                    <label for="client" class="inline_b">по</label>
                        <label>
                            <input type='text' class="form-control dateTo" value=""
                                   name="dateTo"
                                   required/>
                        </label>
                        <span class="input-group-addon">
                                <span class="glyphicon glyphicon-remove"></span>
                            </span>
                        <span class="input-group-addon d_non">
                                <span class="glyphicon glyphicon-th"></span>
                            </span>
                        <input type="hidden" value=""/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="text_left">
                <button type="submit" class="btn btn-primary">Старт</button>
                <button type="reset" class="btn btn-danger">Очистить</button>
            </div>
        </div>

    </form>

    <hr>
    <div class="result"></div>
</div>
</body>
</html>

<script src="src/js/jquery-3.3.1.min.js"></script>
<script src="src/js/bootstrap.min.js"></script>

<script src="src/js/bootstrap-datetimepicker.min.js"></script>
<script src="src/js/bootstrap-datetimepicker.ru.js"></script>

<script src="src/js/moment-with-locales.min.js"></script>

<script>
    $(function () {
        let to = moment();
        // .locale('ru').format('DD.MM.YYYY HH:mm');
        let from = moment(to).add(-30, 'd');

        document.querySelector('#dateFrom input.dateFrom').value = from.locale('ru').format('DD.MM.YYYY HH:mm');
        document.querySelector('#dateTo input.dateTo').value = to.locale('ru').format('DD.MM.YYYY HH:mm');


        $("form[name='form']").submit(function (event) {
            event.preventDefault();

            let form = $(this);

            $.ajax({
                url: "ajax.php",
                data: form.serialize(),
                dataType: 'html'
            }).done(function (result) {
                $('.result').html(result);
            });
        });

        $('#dateFrom').datetimepicker({
            language: 'ru',
            format: 'dd.mm.yyyy hh:ii',
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            forceParse: 0,
            pickerPosition: "bottom-left"
        });

        $('#dateTo').datetimepicker({
            language: 'ru',
            format: 'dd.mm.yyyy hh:ii',
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            forceParse: 0,
            pickerPosition: "bottom-left"
        });

    });

</script>