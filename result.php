<?php

/** @var array $result */
if (!empty($result['data'])) { ?>
    <table class="table table-sm table-striped table-hover">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Дата и время звонка</th>
            <th scope="col">Кто звонил</th>
            <th scope="col">Кому звонили</th>
<!--            <th scope="col">dstchannel</th>-->
            <th scope="col">Продолжительность разговора, сек</th>
            <th scope="col">Запись</th>
        </tr>
        </thead>
        <tbody>

        <?php $i = 1; ?>
        <?php foreach ($result['data'] as $item) { ?>
            <tr>
                <th scope="row"><?php echo $i++ ?></th>
                <td><?= date("d.m.Y H:i:s", strtotime($item['calldate'])) ?></td>
                <td><?= $item['src'] ?></td>
                <td><?= $item['dst'] ?></td>
<!--                <td>--><?//= $item['dstchannel'] ?><!--</td>-->
                <td class="text_cent"><?= $item['billsec'] ?></td>
                <td class="text_cent">
                    <a href="<?= $item['urlrecord'] ?>" target="_blank">file</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php } else { ?>
    <h4>Ничего не найдено...</h4>
<?php }
